# Generated by Django 2.0.7 on 2018-07-18 09:56

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('ecomapp', '0009_auto_20180717_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='date_of_receiving',
            field=models.DateField(default=datetime.datetime(2018, 7, 18, 9, 56, 35, 47325, tzinfo=utc)),
        ),
        migrations.RemoveField(
            model_name='order',
            name='items',
        ),
        migrations.AddField(
            model_name='order',
            name='items',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ecomapp.Cart'),
        ),
    ]
