from django.db import models
from django.db.models.signals import pre_save
from django.contrib.auth.models import User
from django.utils.text import slugify
from transliterate import translit # can translit words from kirill to latin
from django.urls import reverse
from decimal import Decimal
from django.utils import timezone


# Function to save a picture to a specific folder with a readable name
def image_folder(instance, filename):
    filename = instance.slug + '.' + filename.split('.')[1]
    return "{0}/{1}".format(instance.slug, filename)


def big_image_folder(instance, filename):
    filename = 'big-' + instance.slug + '.' + filename.split('.')[1]
    return "{0}/{1}".format(instance.slug, filename)


def pre_save_category_slug(sender, instance, *args, **kwargs):
    if not instance.slug:
        slug = slugify(translit(instance.name, reversed=True)) # translit kirill to latin
        instance.slug = slug

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(blank=True) # can be empty, it is need for pre_save

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'category_slug': self.slug})


# connection function pre_save_category_slug and model Category
pre_save.connect(pre_save_category_slug, sender=Category)


class Brand(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


# redefinition standart manager
class ProductManager(models.Manager):
    def all(self, *arg, **kwargs):
        return super(ProductManager, self).get_queryset().filter(available=True)


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    slug = models.SlugField()
    description = models.TextField()
    image = models.ImageField(upload_to=image_folder) # need to install pillow
    big_image = models.ImageField(upload_to=big_image_folder, blank=True)
    price = models.DecimalField(max_digits=9, decimal_places=2) # max_digits - count of digits in number, decimal_places - count of digits after comma
    available = models.BooleanField(default=True) # if default - product is available
    objects = ProductManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('product_detail', kwargs={'product_slug': self.slug})


class CartItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    item_total = models.DecimalField(max_digits=9, decimal_places=2, default=0.00)

    def __str__(self):
        return "Cart item for product {0}".format(self.product.title)


class Cart(models.Model):
    items = models.ManyToManyField(CartItem, blank=True)
    cart_total = models.DecimalField(max_digits=9, decimal_places=2, default=0.00)

    def __str__(self):
        return str(self.id)

    def add_to_cart(self, product):
        check = True
        for cart_item in self.items.all():
            if cart_item.product.slug == product.slug:
                check = False
                break
        if check:
            new_item, _ = CartItem.objects.get_or_create(product=product, item_total=product.price)
            self.items.add(new_item)
            self.save()

    def remove_from_cart(self, product):
        for cart_item in self.items.all():
            if cart_item.product == product:
                self.items.remove(cart_item)
                self.save()

    def change_quantity(self, cart_item, quantity):
        cart_item.quantity = int(quantity)
        cart_item.item_total = int(quantity) * Decimal(cart_item.product.price)
        cart_item.save()
        new_cart_total = 0.00
        for item in self.items.all():
            new_cart_total += float(item.item_total)
        self.cart_total = new_cart_total
        self.save()


ORDER_STATUS_CHOICES = (
    ('Принят в обработку', 'Принят в обработку'),
    ('Выполняется', 'Выполняется'),
    ('Оплачен', 'Оплачен')
)


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    items = models.ForeignKey(Cart, on_delete=models.CASCADE, default=Cart().save())
    total = models.DecimalField(max_digits=9, decimal_places=2, default=0.00)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=255)
    buying_type = models.CharField(max_length=40, choices=(('Самовывоз', 'Самовывоз'), ('Доставка', 'Доставка')),
                                   default='Самовывоз')
    date = models.DateTimeField(auto_now_add=True)
    date_of_receiving = models.DateField(default=timezone.now())
    comments = models.TextField()
    status = models.CharField(max_length=100, choices=ORDER_STATUS_CHOICES, default=ORDER_STATUS_CHOICES[0][0])

    def __str__(self):
        return "Заказ №{0}".format(str(self.id))